### Return Codes of Errors
  <details open>
    <summary> </summary>

  #### Return Codes

  ```java
   HTTP `200` return code is used when the Succesful responce;
   HTTP `401` return code is used when the request made by Unauthorized user;
   HTTP `403` return code is used when the request made to Forbidden data from user without necessary permissions;
   HTTP `404` return code is used when the request made with undefined data and service can Not Found data for response.
  ```
  #### Errors messages:
  **"Invalid payload"** - this error occurs when the data that was provided in the body of the request doesn't match the base64-decoded payload.
  ```java
  {
       "message": [
           [
               "Invalid payload."
           ]
       ],
       "result": [],
       "success": false
  }
  ```
  **"Nonce not provided."** - this error occurs if your request is missing "nonce" in the request body.
  ```java
  {
      "message": [
          [
              "Nonce not provided."
          ]
      ],
      "result": [],
      "success": false
  }
  ```
  **"Request not provided."** - this error occurs if your request is missing "request" path in the request body.
  ```java
  {
      "message": [
          [
              "Request not provided."
          ]
      ],
      "result": [],
      "success": false
  }
  ```
  **"Unauthorized request."** - this error occurs if the request was signed incorrectly.
  ```java
  {
      "message": [
          [
              "Unauthorized request."
          ]
      ],
      "result": [],
      "success": false
  }
  ```
  **"This action is unauthorized. Enable your key in API settings"** - this error occurs when you are using disabled API key.
  ```java
  {
      "message": [
          [
              "This action is unauthorized. Enable your key in API settings"
          ]
      ],
      "result": [],
      "success": false
  }
  ```
  **"Invalid payload"** - this error occurs when the data that was provided in the body of the request doesn't match the base64-decoded payload.
  ```java
  {
       "message": [
           [
               "Invalid payload."
           ]
       ],
       "result": [],
       "success": false
  }
  ```
  </details>

---

### Public Methods
<details open>
  <summary>
These methods provide information via GET.
The response will return all the information that was posted by the platform.
To obtain private information, use the same methods via POST as an authorized user using api keys.
  </summary>

#### List of Public Pairs
  <details open>
  <summary>
  This method provides information via GET
  </summary>

  ```java
  GET /api/v1/public/markets
  ```

  ```java
  curl -X GET "https://api.tidex.com/api/v1/public/markets" -H "accept: application/json"
  ```

**Response Parameters:**

Name | Type |
------------ | ------------
name | STRING |
moneyPrec | NUMERIC |
stock | STRING | NO |
money | STRING |
stockPrec | NUMERIC |
feePrec | NUMERIC |
minAmount | STRING |

**Response samples:**
```javascript
{
  "code": 0,
  "message": {},
  "result":
  [
    {
      "name":"BCH_BTC",
      "moneyPrec":8,
      "stock":"BCH",
      "money":"BTC",
      "stockPrec":8,
      "feePrec":8,
      "minAmount":"0.001"
    },
    {
      ...
    },
  ],
}
```
  </details>

#### List of Public Tickers
<details open>
  <summary>
  This method provides information via GET
  </summary>

```java
GET /api/v1/public/tickers
```

```java
curl -X GET "https://api.tidex.com/api/v1/public/tickers" -H "accept: application/json"
```

**Response Parameters:**

Name | Type |
---- | ---- |
name | STRING |
bid | STRING |
ask | STRING |
open | STRING |
high | STRING |
low | STRING |
last | STRING |
volume | STRING |
deal | STRING |
change | STRING |

**Response samples:**

```javascript
{
    "code": 200,
    "success": true,
    "message": "",
    "result": {
        "ETH_UAH": {
            "at": 1650542464,
            "ticker": {
                "name": "ETH_UAH",
                "bid": "101275.50569145",
                "ask": "101564.13835842",
                "open": "100285.81312762",
                "high": "101579.95341111",
                "low": "97265.91569436",
                "last": "101335.47031808",
                "vol": "13.78367282",
                "deal": "1365777.629861316184",
                "change": "1"
            }
        }
    }
}
```

</details>


#### Specific Public Ticker Data
  <details open>
  <summary>
  This method provides information via GET
  </summary>

```java
GET /api/v1/public/ticker
```
```java
curl -X GET "https://api.tidex.com/api/v1/public/ticker?market=BTC_USDT" -H "accept: application/json"
```

**Request Parameters:**

Name | Type | Mandatory | Description |
------------ | ------------ | ------------ | ------------ |
market | STRING | YES | Any Public Market

**Response Parameters:**

Name | Type |
---- | ---- |
name | STRING |
bid | STRING |
ask | STRING |
open | STRING |
high | STRING |
low | STRING |
last | STRING |
volume | STRING |
deal | STRING |
change | STRING |

**Response samples:**
```javascript
{
  "code": 0,
  "message": {},
  "result":
  [
    {
      "name":"ETH_BTC",
      "bid":"0.05075249",
      "ask":"0.05075451",
      "open":"0.04799996",
      "high":"0.05086171",
      "low":"0.047891",
      "last":"0.05068996",
      "volume":"71.7808779340174791",
      "deal":"1446.73007651",
      "change":"5"
    },
  ],
}
```
  </details>


#### List of Order Book
<details open>
  <summary>
  This method provides information via GET and POST
  </summary>

```java
GET /api/v1/public/book
```
```java
curl -X GET "https://api.tidex.com/api/v1/public/book?market=BTC_USDT&side=sell&offset=0&limit=1" -H "accept: application/json"
```

**Request Parameters:**

Name | Type | Mandatory | Description |
------------ | ------------ | ------------ | ------------ |
market | STRING | YES | Any Public Market
side | STRING | YES |  buy / sell
offset | NUMERIC | NO | Default 0
limit | NUMERIC | NO | Default 50; min 1; max 1000


**Request in body POST method:**

```java
{
  "market": "ETH_BTC"
  "side": "sell"
  "offset": 0
  "limit": 100
  "request": "/api/v1/public/book",
  "nonce": 1636733702330
}
```

**Response Parameters:**

Name | Type |
------------ | ------------
id | NUMERIC |
market | STRING |
price | STRING |
amount | STRING |
left | STRING |
type | STRING |
side | STRING |
timestamp | NUMERIC |
dealFee | STRING |
takerFee | STRING |
dealStock | STRING |
dealMoney | STRING |

**Response samples:**
```javascript
{
"code":200,
"success":true,
"message":"",
"result":
   {
    "offset":0,
    "limit":1,
    "total":337,
    "orders":
    [
      {
        "id":324101293,
        "market":"ETH_BTC",
        "price":"0.05065051",
        "amount":"1.462",
        "left":"1.462",
        "type":"limit",
        "side":"sell",
        "timestamp":1619691495.549000,
        "dealFee":"0",
        "takerFee":"0",
        "makerFee":"0",
        "dealStock":"0",
        "dealMoney":"0"
      },
      {
        ...
      },
    ],
  },
}
```
</details>

#### Market History
<details open>
  <summary>
  This method provides information via GET and POST
  </summary>

```java
GET /api/v1/public/history
```
```java
curl -X GET "https://api.tidex.com/api/v1/public/history?market=BTC_USDT&lastId=1&limit=10'" -H "accept: application/json"
```

**Request Parameters:**

Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
market | STRING | YES | Any Public Market
lastId | NUMERIC | YES |  last Id
limit | NUMERIC | NO | Default 50; min 1; max 1000


**Request in body POST method:**
```javascript
{
  "market":"BTC_USDT",
  "lastId":1,
  "limit":10,
  "request":"/api/v1/public/history",
  "nonce":1650541053626
}
```

**Response samples:**
```javascript
    "response": {
        "success": true,
        "message": "",
        "result": [
            {
                "id": 265,
                "time": 1533292963.202649,
                "price": "2",
                "type": "sell",
                "amount": "1"
            },
            {
                "id": 263,
                "time": 1533292880.088096,
                "price": "2",
                "type": "sell",
                "amount": "1"
            },
						{...}
    ]
}
```

</details>

#### Market History Data
<details open>
  <summary>
  This method provides information via GET and POST
  </summary>

```java
GET /api/v1/public/history/result
```
```java
curl -X GET "https://api.tidex.com/api/v1/public/history/result?market=BTC_USDT&since=1&limit=20" -H "accept: application/json"
```

**Request Parameters:**
Name | Type | Mandatory | Description |
------------ | ------------ | ------------ | ------------ |
market | STRING | YES | Any Public Market
since | NUMERIC | YES |  Min 1; Market History Since Selected `tid`
limit | NUMERIC | NO | Default 50; min 1; max 1000


**Request in body POST method:**
```java
{
  "market":"LTC_ETH",
  "since":1,
  "limit":20,
  "request":"/api/v1/public/history/result",
  "nonce":1650541233074
}
```

**Response Parameters:**
Name | Type |
------------ | ------------ |
tid | NUMERIC |
date | NUMERIC |
price | STRING |
type | STRING |
amount | STRING |
total | STRING |

**Response:**
```javascript
[
  {
    "tid":23914004,
    "date":1619693164,
    "price":"0.05065949",
    "type":"buy",
    "amount":"0.01854886",
    "total":"0.00093968"
  },
  {
   ...
  },
]
```

</details>

#### Public Products List
<details open>
  <summary>
  This method provides information via GET
  </summary>

```java
GET /api/v1/public/products
```
```java
curl -X GET "https://api.tidex.com/api/v1/public/products" -H "accept: application/json"
```

**Response:**
```javascript
response: {
  success: true,
  message: "",
  result: [
			  {
          "id": "ETH_BTC",
          "fromSymbol": "ETH",
          "toSymbol": "BTC"
        },
        {
           "id": "OCC_BTC",
           "fromSymbol": "OCC",
           "toSymbol": "BTC"
        },
				{...}
	   ]
 }
```
</details>

#### Public Symbols List
<details open>
  <summary>
  This method provides information via GET and POST
  </summary>

```java
GET /api/v1/public/symbols
```
```java
curl -X GET "https://api.tidex.com/api/v1/public/symbols" -H "accept: application/json"
```

**Response Parameters:**
Name | Type |
----- | ----- |
Pair | STRING |

Request in body POST method:
```java
{
  "request":"/api/v1/public/symbols",
  "nonce":1650541438785
}
```

**Response:**
```javascript
{
"code":200,
"success":true,
"message":"",
"result":
    [
      "AAVE_BTC",
      "BCH_BTC",
      "BCH_USD",
      "BNB_BTC",
      "BNB_ETH",
      "BNB_USD",
      "BNB_USDT",
    ]
}
```
</details>

#### Depth List
<details open>
  <summary>
  This method provides information via GET and POST
  </summary>

```java
GET /api/v1/public/depth/result
```
```java
curl -X GET "https://api.tidex.com/api/v1/public/depth/result?market=BTC_USDT&limit=5" -H "accept: application/json"
```

**Request Parameters:**

Name | Type | Mandatory | Description |
------------ | ------------ | ------------ | ------------ |
market | STRING | YES | Any Public Market
limit | NUMERIC | NO | Default 50; min 1; max 1000


**Request in body POST method:**
```JSON
{
  "market": "ETH_BTC",
  "limit": 100,
  "request": "/api/v1/public/depth/result",
  "nonce": 1650541438785
}
```

**Response Parameters:**

Name | Type | Description
------------ | ------------ | ------------
type | STRING | asks (sell) / bids (buy)
price | STRING | 1st parametr - in left side currency
amount | STRING |2st parametr - in left side currency

**Response:**
```java
{
  "asks":
    [
      [
        "0.05084851", //price
        "21.437"      //amount
      ],
      [
        "0.05084951", //price
        "0.453"       //amount
      ]
    ],
  "bids":
    [
      [
        "0.05084649", //price
        "3.99"        //amount
      ],
      [
        "0.05084549", //price
        "18.15"       //amount
      ]
    ]
}
```

</details>

#### List of Graphic Data KLine
<details open>
  <summary>
  This method provides information via GET and POST
  </summary>

```java
GET /api/v1/public/kline
```
```java
curl -X GET "https://api.tidex.com/api/v1/public/kline?market=BTC_USDT&start=1650210446&end=1650390446&interval=60" -H "accept: application/json"
```

**Request Parameters:**
Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
market | STRING | YES | Any Public Market
start | NUMERIC | YES |  unixtime from
end | NUMERIC | YES | unixtime to
interval | NUMERIC | NO | Default 60; min 60; max 604800 sec

**Request in body POST method:**

```javascript
{
  "market": "ETH_BTC",
  "start": 1601359005,
  "end": 1601389005,
  "interval": 60,
  "request": "/api/v1/public/kline",
  "nonce": 1650541438785
}
```

**Response Parameters:**

Name | Type |
------------ | ------------
time | NUMERIC |
open | STRING |
close | STRING |
highest | STRING |
lowest | STRING |
volume | STRING |
amount | STRING |
market | STRING |

**Response:**
```javascript
{
"code":200,
"success":true,
"message":"",
"result":
  {
    "market":"ETH_BTC",
    "start":1619695680,
    "end":1619699280,
    "interval":60,
    "kline":
      [
        {
          "time":1619695680,
          "open":"0.05068385",
          "close":"0.05068196",
          "highest":"0.05069765",
          "lowest":"0.0506626",
          "volume":"0.45754085",
          "amount":"0.0231873341977513",
          "market":"ETH_BTC"
        },
        {
          ...
        },
     ]
  }
}
```
</details>

</details>

---

### Private Data Methods
<details open>
<summary>
First of all to use POST methods check how to made [HTTP Authorization](#http-authorization)
Use next methods via POST and obtain full information
All of this methods can be use only with POST. Before using check [HTTP Authorization](#http-authorization)
</summary>

#### 1. Market API
<details open>
<summary> </summary>

  #### Create new Limit Order
  <details open>
  <summary>
  This method provides information via POST
  </summary>

  ```java
  POST /api/v1/order/new
  ```
  ```java
  curl --location --request POST 'https://api.tidex.com/api/v1/order/new' \
--header 'Content-Type': 'application/json' \
--header 'X-TXC-APIKEY': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-PAYLOAD': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-SIGNATURE': '0000000000XXXXXXXXXXXXXXXXX' \
--data-raw '{"market":"BTC_USDT","lastId":1,"limit":10,"request":"/api/v1/public/history","nonce":1650473825200}'
```

**Request Parameters:**

Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
request | STRING | YES | A request path without the domain name
market | STRING | YES | Any Market
side | STRING | YES |  sell or buy
amount | STRING | YES | Order amount in 1st Ticker of Pair
price | STRING | YES | Order price in 2st Ticker of Pair
nonce | STRING | YES | A number that is always greater than the previous request’s nonce number

**Request:**

```javascript
{
  "request": "/api/v1/order/new",
  "market": "ETH_BTC",
  "side" : "sell",
  "amount" : "0.1",
  "price" : "0.1",
  "nonce": 1636733702330
}
```

**Response Parameters:**

Name | Type  | Description
------------ | ------------ | ------------
orderId | STRING  | ID of placed order
market | STRING  |  Market of placed order
price | STRING  | Price of placed order
side | STRING  | Side of placed order
type | STRING  | Type of placed order
timestamp | NUMERIC  | Time of placed order
dealMoney | STRING  | Order Amount in 2st Ticker of Pair
dealStock | STRING  | Order Amount in 1st Ticker of Pair
amount | STRING  | Order Amount in 1st Ticker of Pair
takerFee | STRING  | Order Taker Fee
makerFee | STRING  | Order Maker Fee
left | STRING  | Order left amount - shows how much left in order; if = 0 - order finished
dealFee | STRING  | Deal Fee of Order


**Response:**
```javascript
{
    "success": true,
    "message": "",
    "result": [
        "orderId": 25749,
        "market": "ETH_BTC",
        "price": "0.1",
        "side": "sell",
        "type": "limit",
        "timestamp": 1537535284.828868,
        "dealMoney": "0",
        "dealStock": "0",
        "amount": "0.1",
        "takerFee": "0.002",
        "makerFee": "0.002",
        "left": "0.1",
        "dealFee": "0"
    ]
}
```

</details>

#### Cancel Order
<details open>
  <summary>
  This method provides information via POST
  </summary>

```java
POST /api/v1/order/cancel
```
```java
curl --location --request POST 'https://api.tidex.com/api/v1/order/cancel' \
--header 'Content-Type': 'application/json' \
--header 'X-TXC-APIKEY': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-PAYLOAD': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-SIGNATURE': '0000000000XXXXXXXXXXXXXXXXX' \
--data-raw '{"market":"BTC_USDT","lastId":1,"limit":10,"request":"/api/v1/public/history","nonce":1650473825200}'
```

**Request Parameters:**

Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
request | STRING | YES | A request path without the domain name
market | STRING | YES | Any Market
orderId | STRING | YES |  Order ID for canceling
nonce | STRING | YES | A number that is always greater than the previous request’s nonce number

**Request:**

```javascript
{
  "request": "/api/v1/order/cancel",
  "market": "ETH_BTC",
  "orderId": 25749,
  "nonce": 1636733702330
}
```

**Response Parameters:**

Name | Type  | Description
------------ | ------------ | ------------
orderId | STRING  | ID of placed order
market | STRING  |  Market of placed order
price | STRING  | Price of placed order
side | STRING  | Side of placed order
type | STRING  | Type of placed order
timestamp | NUMERIC  | Time of placed order
dealMoney | STRING  | Order Amount in 2st Ticker of Pair
dealStock | STRING  | Order Amount in 1st Ticker of Pair
amount | STRING  | Order Amount in 1st Ticker of Pair
takerFee | STRING  | Order Taker Fee
makerFee | STRING  | Order Maker Fee
left | STRING  | Order left amount - shows how much left in order; if = 0 - order finished
dealFee | STRING  | Deal Fee of Order


**Response:**
```javascript
{
    "success": true,
    "message": "",
    "result": [
        "orderId": 25749,
        "market": "ETH_BTC",
        "price": "0.1",
        "side": "sell",
        "type": "limit",
        "timestamp": 1537535284.828868,
        "dealMoney": "0",
        "dealStock": "0",
        "amount": "0.1",
        "takerFee": "0.002",
        "makerFee": "0.002",
        "left": "0.1",
        "dealFee": "0"
    ]
}
```

</details>

#### My Active Orders
  <details open>
  <summary>
  This method provides information via POST
  </summary>

```java
POST /api/v1/orders
```
```java
curl --location --request POST 'https://api.tidex.com/api/v1/orders' \
--header 'X-TXC-APIKEY': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-PAYLOAD': '0000000000XXXXXXXXXXXXXXXXX==' \
--header 'X-TXC-SIGNATURE': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'Content-Type': 'application/json' \
--data-raw '{"market":"BTC_USDT","lastId":1,"limit":10,"request":"/api/v1/public/history","nonce":1650473825200}'
```

**Request Parameters:**

Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
request | STRING | YES | A request path without the domain name
market | STRING | YES | Any Market
offset | STRING | NO |  Default: 0
limit | NUMERIC | NO |  Default: 50
nonce | STRING | YES | A number that is always greater than the previous request’s nonce number

**Request:**

```javascript
{
  "request": "/api/v1/orders",
  "market": "ETH_BTC",
  "offset": 10,
  "limit": 100,
  "nonce": 1636733702330
}
```

**Response Parameters:**

Name | Type  | Description
------------ | ------------ | ------------
orderId | STRING  | ID of placed order
market | STRING  |  Market of placed order
price | STRING  | Price of placed order
side | STRING  | Side of placed order
type | STRING  | Type of placed order
timestamp | NUMERIC  | Time of placed order
dealMoney | STRING  | Order Amount in 2st Ticker of Pair
dealStock | STRING  | Order Amount in 1st Ticker of Pair
amount | STRING  | Order Amount in 1st Ticker of Pair
takerFee | STRING  | Order Taker Fee
makerFee | STRING  | Order Maker Fee
left | STRING  | Order left amount - shows how much left in order; if = 0 - order finished
dealFee | STRING  | Deal Fee of Order

**Response:**
```javascript
{
    "success": true,
    "message": "",
    "result":
      {
          "limit": 100,
          "offset": 10,
          "total": 17,
          "result":
            [
	       {
                 "id": 9472,
                 "left": "1",
                 "market": "ETH_BTC",
                 "amount": "1",
                 "type": "limit" | "market",
                 "price": "0.01",
                 "timestamp": 1533561772.211871,
                 "side": "sell" | "buy",
                 "dealFee": "0",
                 "takerFee": "0",
                 "makerFee": "0",
                 "dealStock": "0",
                 "dealMoney": "0"
               },
               {
	         ...
	       },
            ]
      }
}
```
</details>

</details>

#### 2. Account API
<details open>
<summary>
</summary>

#### My All Trade Balances
<details open>
  <summary>
  This method provides information via POST
  </summary>

```java
POST /api/v1/account/balances
```
```java
curl --location --request POST 'https://api.tidex.com/api/v1/account/balances' \
--header 'X-TXC-APIKEY': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-PAYLOAD': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-SIGNATURE': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'Content-Type': 'application/json' \
--data-raw '{"market":"BTC_USDT","lastId":1,"limit":10,"request":"/api/v1/public/history","nonce":1650473825200}'
```

**Request Parameters:**
Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
request | STRING | YES | A request path without the domain name
nonce | STRING | YES | A number that is always greater than the previous request’s nonce number

**Request:**
```javascript
{
  "request": "/api/v1/account/balances",
  "nonce": 1636733702330
}
```

**Response Parameters:**

Name | Type  | Description
------------ | ------------ | ------------
available | NUMERIC | Amount without active orders
freeze | STRING | active orders amount

**Response:**
```javascript
{
    "success": true,
    "message": "",
    "result":
       {
            "ATB":
	    	{
                   "available": "0",
                   "freeze": "0"
            	},
            "USD":
	    	{
                   "available": "8990",
                   "freeze": "0"
            	},
            	{
	           ...
	    	},
       }
}
```
</details>

#### My Specific Trade Balance
<details open>
  <summary>
  This method provides information via POST
  </summary>

```java
POST /api/v1/account/balance
```
```java
curl --location --request POST 'https://api.tidex.com/api/v1/account/balance' \
--header 'X-TXC-APIKEY': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-PAYLOAD': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-SIGNATURE': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'Content-Type': 'application/json' \
--data-raw '{"market":"BTC_USDT","lastId":1,"limit":10,"request":"/api/v1/public/history","nonce":1650473825200}'
```

**Request Parameters:**
Name | Type  |
------------ | ------------
request | STRING | YES | A request path without the domain name
currency | STRING
nonce | STRING | YES | A number that is always greater than the previous request’s nonce number

**Request:**
```javascript
{
  "request": "/api/v1/account/balance",
  "currency": "ETH",
  "nonce": 1636733702330
}
```
**Response Parameters:**
Name | Type  | Description
------------ | ------------ | ------------
available | NUMERIC | Amount without active orders
freeze | STRING | active orders amount

**Response:**
```javascript
{
    "success": true,
    "message": "",
    "result":
    	{
            "available": "8990",
            "freeze": "0"
        }
}
```
</details>

#### Get My Order Info
<details open>
  <summary>
  This method provides information via POST
  </summary>

```java
POST /api/v1/account/order
```
```java
curl --location --request POST 'https://api.tidex.com/api/v1/account/order' \
--header 'X-TXC-APIKEY': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-PAYLOAD': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-SIGNATURE': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'Content-Type': 'application/json' \
--data-raw '{"market":"BTC_USDT","lastId":1,"limit":10,"request":"/api/v1/public/history","nonce":1650473825200}'
```

**Request Parameters:**
Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
request | STRING | YES | A request path without the domain name
market | STRING | YES | Any Market
offset | STRING | NO |  Default: 0
limit | NUMERIC | NO |  Default: 50
nonce | STRING | YES | A number that is always greater than the previous request’s nonce number

**Request:**

```javascript
{
  "request": "/api/v1/account/order",
  "market": "ETH_BTC",
  "offset": 10,
  "limit": 100,
  "nonce": 1636733702330
}
```

**Response Parameters:**

Name | Type  | Description
------------ | ------------ | ------------
time | NUMERIC  | Trade Time
fee | STRING  | Trade Fee
price | STRING  | Trade Price
amount | STRING  | Trade amount in 1st Ticker of Pair
Id | NUMERIC  | User ID
dealOrderId | NUMERIC  | Trade ID
role | NUMERIC  | Trade Role: Taker or Maker
deal | STRING  | Trade amount in 2st Ticker of Pair.


**Response:**
```javascript
{
    "success": true,
    "message": "",
    "result":
        {
          "offset": 0,
          "limit": 50,
          "records":
            {
              "time": 1533310924.935978,
              "fee": "0",
              "price": "80.22761599",
              "amount": "2.12687945",
              "id": 548,
              "dealOrderId": 1237,
              "role": 1,
              "deal": "170.6344677716224055"
            }
        }
}
```
</details>

#### Get My Trades Info
<details open>
  <summary>
  This method provides information via POST
  </summary>

```java
POST /api/v1/account/trades
```
```java
curl --location --request POST 'https://api.tidex.com/api/v1/account/trades' \
--header 'X-TXC-APIKEY': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-PAYLOAD': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-SIGNATURE': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'Content-Type': 'application/json' \
--data-raw '{"market":"BTC_USDT","lastId":1,"limit":10,"request":"/api/v1/public/history","nonce":1650473825200}'
```

**Request Parameters:**
Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
request | STRING | YES | A request path without the domain name
market | STRING | YES | Any Market
offset | STRING | NO |  Default: 0
limit | NUMERIC | NO |  Default: 50
nonce | STRING | YES | A number that is always greater than the previous request’s nonce number

**Request:**
```javascript
{
  "request": "/api/v1/account/order",
  "market": "ETH_BTC",
  "offset": 10,
  "limit": 100,
  "nonce": 1636733702330
}
```

**Response Parameters:**

Name | Type  | Description
------------ | ------------ | ------------
time | NUMERIC  | Trade Time
fee | STRING  | Trade Fee
price | STRING  | Trade Price
amount | STRING  | Trade amount in 1st Ticker of Pair
Id | NUMERIC  | User ID
dealOrderId | NUMERIC  | Trade ID
role | NUMERIC  | Trade Role: Taker or Maker
deal | STRING  | Trade amount in 2st Ticker of Pair.


**Response:**
```javascript
{
    "success": true,
    "message": "",
    "result":
        {
          "offset": 0,
          "limit": 50,
          "records":
            {
              "time": 1533310924.935978,
              "fee": "0",
              "price": "80.22761599",
              "amount": "2.12687945",
              "id": 548,
              "dealOrderId": 1237,
              "role": 1,
              "deal": "170.6344677716224055"
            }
        }
}
```
</details>

#### My Order History
<details open>
  <summary>
  This method provides information via POST
  </summary>

```java
POST /api/v1/account/order_history_list
```
```java
curl --location --request POST 'https://api.tidex.com/api/v1/account/order_history' \
--header 'X-TXC-APIKEY': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-PAYLOAD': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'X-TXC-SIGNATURE': '0000000000XXXXXXXXXXXXXXXXX' \
--header 'Content-Type': 'application/json' \
--header 'Cookie': 'JSESSIONID=280523DF04A70E6B5' \
--data-raw '{"request":"/api/v1/account/order_history","offset":0,"limit":100,"nonce":1650536118818}'
```

**Request Parameters:**

Name | Type | Mandatory | Description
------------ | ------------ | ------------ | ------------
request | STRING | YES | A request path without the domain name
market | STRING | YES | Any Market
offset | STRING | NO |  Default: 0
limit | NUMERIC | NO |  Default: 50
nonce | STRING | YES | A number that is always greater than the previous request’s nonce number

**Request:**

```javascript
{
  "request": "/api/v1/account/order_history_list",
  "market": "ETH_BTC",
  "offset": 10,
  "limit": 100,
  "nonce": 1636733702330
}
```

**Response Parameters:**

Name | Type  | Description
------------ | ------------ | ------------
amount | STRING  | Order Amount in 1st Ticker of Pair
price | STRING  | Price of placed order
type | STRING  | Type of placed order
Id | STRING  | ID of placed order
side | STRING  | Side of placed order
ctime | NUMERIC  | Time of create order
takerFee | STRING  | Order Taker Fee
ftime | NUMERIC  | Time of finishing order
market | STRING  |  Market of placed order
makerFee | STRING  | Order Maker Fee
dealFee | STRING  | Deal Fee of Order
dealStock | STRING  | Order Amount in 1st Ticker of Pair
dealMoney | STRING  | Order Amount in 2st Ticker of Pair
marketName | STRING  |  Market of placed order

**Response:**
```javascript
{
  {
    "success": true,
    "message": "",
    "result":
      {
        "records":
	  [
            {
                "amount": "1",
                "price": "0.01",
                "type": "limit"
                "id": 9740,
                "side": "sell"
                "ctime": 1533568890.583023,
                "takerFee": "0.002",
                "ftime": 1533630652.62185,
                "market": "ETH_BTC",
                "makerFee": "0.002",
                "dealFee": "0.002",
                "dealStock": "1",
                "dealMoney": "0.01",
                "marketName": "ETH_BTC"
            },
            {
	      ...
	    }
	  ]
  }
}
```

</details>
</details>

</details>
</details>

---

### CoinMarketCap
<details open>
  <summary>
  </summary>
  
| Name | Category | Status | Description |
| ------ | ------ | ------ | ------ |
| Summary Endpoint | **[Summary](#summary-endpoint)** | Mandatory | Overview of market data for all tickers and all markets |
| Endpoint A1 | **[Assets](#assets-endpoint)** | Recommended | In depth details on crypto currencies available on the exchange. |
| Endpoint A2| **[Ticker](#ticker-endpoint)** | Mandatory | 24-hour rolling window price change statistics for all markets. |
| Endpoint A3| **[Orderbook](#orderbook-endpoint)** | Mandatory | Market depth of a trading pair. One array containing a list of ask prices and another array containing bid prices. Query for level 2 order book with full depth available as minimum requirement. |
| Endpoint A4| **[Trades](#trades-endpoint)** | Mandatory | Recently completed trades for a given market. 24 hour historical full trades available as minimum requirement. |

#### SUMMARY ENDPOINT

<details open>
  <summary>
  </summary>

```java
GET /api/cmc/v1/summary
```
```java
curl -X GET "https://api.tidex.com/api/cmc/v1/summary" -H "accept: application/json"
```

**Response Parameters:**

| Name | Type | Status |
| ------ | ------ | ------ |
| trading_pairs | string | Mandatory |
| base_currency | string | Recommended |
| quote_currency | string | Recommended |
| last_price | decimal | Mandatory |
| lowest_ask | decimal | Mandatory |
| highest_bid | decimal | Mandatory |
| base_volume | decimal | Mandatory |
| quote_volume | decimal | Mandatory |
| price_change_percent_24h | decimal | Mandatory |
| highest_price_24h | decimal | Mandatory |
| lowest_price_24h | decimal | Mandatory |

**Response**
```javascript
[
   {
        "trading_pairs": "YFI_USDT",
        "base_currency": "YFI",
        "quote_currency": "USDT",
        "last_price": 25477.88119744,
        "lowest_ask": 25492.06,
        "highest_bid": 25455.98,
        "base_volume": 261.8174298,
        "quote_volume": 6637837.775140016199,
        "price_change_percent_24h": -2,
        "highest_price_24h": 26239.192859,
        "lowest_price_24h": 24470.86
    },
    {
      ...
    }
]
```

</details>


#### ASSETS ENDPOINT
<details open>
  <summary>
  </summary>

```java
GET /api/cmc/v1/assets
```
```java
curl -X GET "https://api.tidex.com/api/cmc/v1/assets" -H "accept: application/json"
```

**Response Parameters:**

| Name | Type | Status |
| ------ | ------ | ------ |
| name | string | Recommended |
| unified_cryptoasset_id | integer | Recommended |
| can_withdraw | boolean | Recommended |
| can_deposit | boolean | Recommended |
| min_withdraw | decimal | Recommended |
| max_withdraw | decimal | Recommended |
| maker_fee | decimal | Recommended |
| taker_fee | decimal | Recommended |

**Response**

```javascript
{
    "KNC": {
        "name": "Kyber Network",
        "can_withdraw": true,
        "can_deposit": true,
        "min_withdraw": 0.000000000000000000,
        "max_withdraw": 0.000000000000000000,
        "maker_fee": 0.002,
        "taker_fee": 0.002
    },
    {
     ...
    }
}
```

</details>

#### TICKER ENDPOINT
<details open>
  <summary>
  </summary>

```java
GET /api/cmc/v1/ticker
```
```java
curl -X GET "https://api.tidex.com/api/cmc/v1/ticker" -H "accept: application/json"
```

**Response Parameters:**

| Name | Type | Status |
| ------ | ------ | ------ |
| base_id | integer | Recommended |
| quote_id | integer | Recommended |
| last_price | decimal | Mandatory |
| base_volume | decimal | Mandatory |
| quote_volume | decimal | Mandatory |
| is_frozen | decimal | Recommended |


**Response:**
```javascript
 {
    "QMALL_UAH": {
        "last_price": "1",
        "quote_volume": "0",
        "base_volume": "0",
        "is_frozen": 0
    },
    {
      ...
    }
}
```

</details>

#### ORDERBOOK ENDPOINT
<details open>
  <summary>
  </summary>

```java
GET /api/cmc/v1/orderbook/{market_pair}
```
```java
curl -X GET "https://api.tidex.com/api/cmc/v1/orderbook/BTC_USDT" -H "accept: application/json"
```

**Request Parameters**
| Name | Type | Status | Description
| ------ | ------ | ------ | ------ |
| **market_pair** | string | Mandatory | A pair such as “LTC_BTC” |
| depth | integer | Recommended (used to calculate liquidity score for rankings) | Orders depth quantity: [0,5,10,20,50,100,500] Not defined or 0 = full order book. Depth = 100 means 50 for each bid/ask side. |
| level | integer | Recommended(used to calculate liquidity score for rankings) | Level 1 – Only the best bid and ask. Level 2 – Arranged by best bids and asks. Level 3 – Complete order book, no aggregation. |

**Response Parameters:**
| Name | Type | Status | Description |
| ------ | ------ | ------ |  ------ |
| timestamp | integer | Mandatory | Unix timestamp in milliseconds for when the last updated time occurred |
| bids | decimal | Mandatory | An array containing 2 elements. The offer price and quantity for each bid order |
| asks | decimal | Mandatory | An array containing 2 elements. The ask price and quantity for each ask order. |

**Response:**
```javascript
 {
   "timestamp":"1585177482652",
   "bids":[
      [
         "12462000",
         "0.04548320"
      ],
      [
         "12457000",
         "3.00000000"
      ]
   ],
   "asks":[
      [
         "12506000",
         "2.73042000"
      ],
      [
         "12508000",
         "0.33660000"
      ]
   ]
}
```

</details>

#### TRADES ENDPOINT
<details open>
  <summary>
  </summary>

```java
GET /api/cmc/v1/trades/{market_pair}
```
```java
curl -X GET "https://api.tidex.com/api/cmc/v1/trades/BTC_USDT" -H "accept: application/json"
```

**Request Parameters**
| Name | Type | Status | Description
| ------ | ------ | ------ | ------ |
| market_pair | string | Mandatory | A pair such as “LTC_BTC” |


**Response Parameters:**
| Name | Type | Status |
| ------ | ------ | ------ |
| trade_id | integer | Mandatory |
| price | decimal | Mandatory |
| base_volume | decimal | Mandatory |
| quote_volume | decimal | Mandatory |
| timestamp | integer (UTC timestamp in ms) | Mandatory |
| type | string | Mandatory |

**Response:**
```javascript
 [
    {
        "type": "sell",
        "price": 43504.86451672,
        "base_volume": 0.002487,
        "quote_volume": 108.19659805,
        "trade_id": 171663170,
        "timestamp": 1644584940
    },
    {
     ...
    }
]
```
</details>

</details>

---
